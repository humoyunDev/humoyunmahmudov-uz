import axios from "@/plugins/vuex/axios"
import store from "@/plugins/vuex/store"

axios.defaults.headers.common['Content-Type'] = 'application/ld+json'

axios.interceptors.request.use((config) => {
    if(config.url !== 'https://humoyunmahmudov.uz/api/users/auth') {
        config.headers.common['Authorization'] = 'bearer ' + store.getters.getToken
    }

    return config
})

export default axios