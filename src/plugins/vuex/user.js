import axios from "axios"

export default {
    actions: {
        fetchToken(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('https://api.humoyunmahmudov.uz/api/users/auth', data)
                    .then((response) => {
                        console.log('token olindi')
                        console.log(response)

                        context.commit('updateToken', response.data.token)
                        resolve()
                    })
                    .catch(() => {
                        console.log('token olishda xatolik yuz berdi')
                        reject()
                    })
                    .finally(() => {
                        console.log('Bu funksiya har doim ishlaydi. Yoki then() dan keyin, yoki catch() dan keyin')
                    })
            })
        },
        pushUser(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('https://api.humoyunmahmudov.uz/api/users', data)
                    .then((response) => {
                        console.log('foydalanuvchi qoshildi')
                        console.log(response)

                        // context.commit('updateToken', response.data.token)
                        resolve()
                    })
                    .catch(() => {
                        console.log('foydalanuvchi qoshishda xatolik yuz berdi')
                        reject()
                    })
                    .finally(() => {
                        console.log('Bu funksiya har doim ishlaydi. Yoki then() dan keyin, yoki catch() dan keyin')
                    })
            })
        }
    },
    mutations: {
        updateToken(state, token) {
            localStorage.setItem('token', token)
            state.token = token
        }
    },
    state: {
        token: localStorage.getItem('token')
    },
    getters: {
        getToken(state) {
            return state.token
        }
    },


}